<?php

function bs23_script()
{
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.css', array(), '3.3.6');
    wp_enqueue_style('blog', get_template_directory_uri() . '/css/blog.css');
    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.js', array('jquery'), '3.3.6', true);
}

add_action('wp_enqueue_scripts', 'bs23_script');

function bs23_google_fonts()
{
    wp_register_style('OpenSans', 'http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800');
    wp_enqueue_style('OpenSans');
}

add_action('wp_print_styles', 'bs23_google_fonts');

// WordPress Titles
add_theme_support('title-tag');
// Support Featured Images
add_theme_support('post-thumbnails');
add_image_size('featured', 600, 600, true);
//add_action( 'after_setup_theme', 'prefix_theme_setup' );

function custom_settings_add_menu()
{
    add_menu_page('Theme Panel', 'Theme Panel', 'manage_options', 'theme_panel', 'theme_settings_page', null, 99);
}

add_action('admin_menu', 'custom_settings_add_menu');

function theme_settings_page()
{
    ?>
    <div class="wrap">
        <h1>Theme Panel</h1>
        <form method="post" action="options.php">
            <?php
            settings_fields("section");
            do_settings_sections("theme-options");
            submit_button();
            ?>
        </form>
    </div>
    <?php
}

function display_twitter_element()
{
    ?>
    <input type="text" name="twitter_url" id="twitter_url" value="<?php echo get_option('twitter_url'); ?>"/>
    <?php
}

function display_facebook_element()
{
    ?>
    <input type="text" name="facebook_url" id="facebook_url" value="<?php echo get_option('facebook_url'); ?>"/>
    <?php
}

function display_layout_element()
{
    ?>
    <input type="checkbox" name="theme_layout" value="1" <?php checked(1, get_option('theme_layout'), true); ?> />
    <?php
}

function logo_display()
{
    ?>
    <input type="file" name="logo"/>
    <?php echo get_option('logo'); ?>
    <?php
}

function handle_logo_upload()
{
    if (!empty($_FILES["demo-file"]["tmp_name"])) {
        $urls = wp_handle_upload($_FILES["logo"], array('test_form' => FALSE));
        $temp = $urls["url"];
        return $temp;
    }

    return $option;
}


function display_theme_panel_fields()
{
    add_settings_section("section", "All Settings", null, "theme-options");

    add_settings_field("twitter_url", "Twitter Profile Url", "display_twitter_element", "theme-options", "section");
    add_settings_field("facebook_url", "Facebook Profile Url", "display_facebook_element", "theme-options", "section");
    add_settings_field("theme_layout", "Do you want the layout to be responsive?", "display_layout_element", "theme-options", "section");
    add_settings_field("logo", "Logo", "logo_display", "theme-options", "section");

    register_setting("section", "logo", "handle_logo_upload");
    register_setting("section", "twitter_url");
    register_setting("section", "facebook_url");
    register_setting("section", "theme_layout");
}

add_action("admin_init", "display_theme_panel_fields");

function create_my_custom_post()
{
    register_post_type('my-custom-post',
        array(
            'labels' => array(
                'name' => __('My Custom Post'),
                'singular_name' => __('My Custom Post'),
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array(
                'title',
                'editor',
                'thumbnail',
                'custom-fields'
            )
        ));
}

add_action('init', 'create_my_custom_post');

//2nd custom post
function create_post_your_post()
{
    register_post_type('your_post',
        array(
            'labels' => array(
                'name' => __('Your Post'),
            ),
            'public' => true,
            'hierarchical' => true,
            'has_archive' => true,
            'supports' => array(
                'title',
                'editor',
                'excerpt',
                'thumbnail',
            ),
            'taxonomies' => array(
                'post_tag',
                'category',

            )
        )
    );
    register_taxonomy_for_object_type('category', 'your_post');
    register_taxonomy_for_object_type('post_tag', 'your_post');
}

add_action('init', 'create_post_your_post');