<?php get_header() ?>
<div class="row">
    <div class="col-sm-8 blog-main">
        <?php
//        global  $query_string;
//        $posts = query_posts($query_string);

        if (have_posts()) :
            while (have_posts()) :
                the_post();
                get_template_part('content', get_post_format());

            endwhile;
            ?>

            <nav>
                <ul class="pager">
                    <li><?php previous_posts_link('Previous' ); ?></li>
                    <li><?php next_posts_link( 'Next' ); ?></li>

                </ul>
            </nav>
        <?php
        endif;

        ?>

    </div><!-- /.blog-main -->
    <?php get_sidebar(); ?>
    <!-- /.blog-sidebar -->
</div><!-- /.row -->
<?php get_footer() ?>
<!--tutorial link -->
<!--https://www.taniarascia.com/developing-a-wordpress-theme-from-scratch/-->
<!--part two-->
<!--https://www.taniarascia.com/wordpress-from-scratch-part-two/-->

<!--problem occured on git push-->
